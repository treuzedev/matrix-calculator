import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*
import kotlin.system.exitProcess

// build a matrix with user input
fun matrixBuilder(size : List<String>) : ArrayList<ArrayList<Double>>
{
    // funtion variables
    val i: Int
    val j: Int
    val matrix = arrayListOf<ArrayList<Double>>()
    var rows: ArrayList<Double>
    val zero : Int
    var input : List<String>


    // initialize values
    zero = 0


    // set matriz size
    i = size[0].toInt()
    j = size[1].toInt()


    // build matrix
    for (row in 0 until i)
    {
        rows = arrayListOf()
        for (column in 0 until j)
        {
            rows.add(zero.toDouble())
        }
        matrix.add(rows)
    }


    // accept user input
    for (index in 0 until matrix.size)
    {
        input = matrixValues(j)
        for (col in 0 until matrix[index].size)
        {
            matrix[index][col] = input[col].toDouble()
        }
    }

    return matrix
}


// user input with error check for matrix values
fun matrixValues(j : Int) : List<String>
{
    // function variables
    var input: List<String>
    var counter : Int


    // set counter to zero, initialize input
    counter = 0
    input = listOf()


    // continuously ask for correct user input
    while(counter < 1)
    {
        print("> ")
        input = readLine()?.split(" ") ?: listOf("none")

        if (input.size != j)
        {
            println("Not enough values in matrix row.")
            counter--
        }
        else if (input[counter].toIntOrNull() == null)
        {
            println("Matrix row values not entered correctly..")
            counter--
        }

        counter++
    }

    return input
}


// print a matrix
fun matrixPrinter(matrix : ArrayList<ArrayList<Double>>)
{
    // function variables
    val df = DecimalFormat("#.##", DecimalFormatSymbols(Locale.US))


    // round every value
    for (row in matrix.indices)
    {
        for (col in matrix[row].indices)
        {
            matrix[row][col] = df.format(matrix[row][col]).toDouble()
        }
    }


    // print matrix
    for (index in matrix.indices)
    {
        println(matrix[index].joinToString(" | "))
    }
}


// multiplicate matrix
fun matrixScalar(scalar : Double, matrix : ArrayList<ArrayList<Double>>) : ArrayList<ArrayList<Double>>
{
    for (row in matrix.indices)
    {
        for (col in matrix[row].indices)
        {
            matrix[row][col] *= scalar
        }
    }

    return matrix
}


// scalar check
fun getScalar() : String
{
    // function variables
    val scalar : String


    // user input
    print("Enter scalar: > ")
    scalar = readLine() ?: "none"


    // check for correct input
    if (scalar.toDoubleOrNull() == null || scalar.isEmpty() || scalar.isBlank() || scalar == "none")
    {
        println("The scalar must be a valid number.")
        dashes()
        return ""
    }

    else
    {
        return scalar
    }
}


// multiply two matrices
fun matrixMultiplication(matrix1 : ArrayList<ArrayList<Double>>,
                         matrix2 : ArrayList<ArrayList<Double>>,
                         matrix1_size : List<String>,
                         matrix2_size : List<String>) : ArrayList<ArrayList<Double>>
{
    // function variables
    val matrix1_rows : Int
    val matrix2_cols : Int
    val matrix = arrayListOf<ArrayList<Double>>()
    var rows : ArrayList<Double>
    val zero : Int


    // initialize variables
    zero = 0


    // build resulting matrix
    matrix1_rows = matrix1_size[0].toInt()
    matrix2_cols = matrix2_size[1].toInt()

    for (row in 0 until matrix1_rows)
    {
        rows = arrayListOf()
        for (column in 0 until matrix2_cols)
        {
            rows.add(zero.toDouble())
        }
        matrix.add(rows)
    }


    // multiply matrices
    for (row in matrix.indices)
    {
        for (col in matrix[row].indices)
        {
            for (row1 in 0 until matrix2.size)
            {
                matrix[row][col] += matrix1[row][row1] * matrix2[row1][col]
            }
        }
    }

    return matrix

}


// transpose matrix based on input
fun transposeMatrix(option : Int, size : List<String>, matrix : ArrayList<ArrayList<Double>>) : ArrayList<ArrayList<Double>>
{
    // function variables
    val i : String
    val j : String
    val matrix_size : List<String>
    val transposed_matrix : ArrayList<ArrayList<Double>>
    val row_size : Int
    val col_size : Int


    if (option == 1) // normal transpose
    {
        i = size[1]
        j = size[0]
        matrix_size = listOf<String>(i, j)
        transposed_matrix = noInputMatrixBuilder(matrix_size)

        for (row in transposed_matrix.indices)
        {
            for (col in transposed_matrix[row].indices)
            {
                transposed_matrix[row][col] = matrix[col][row]
            }
        }
        return transposed_matrix
    }
    else if (option == 2)
    {
        i = size[1]
        j = size[0]
        matrix_size = listOf<String>(i, j)
        transposed_matrix = noInputMatrixBuilder(matrix_size)
        row_size = size[0].toInt() - 1
        col_size = size[1].toInt() - 1

        for (row in transposed_matrix.indices)
        {
            for (col in transposed_matrix[row].indices)
            {
                transposed_matrix[row][col] = matrix[row_size - col][col_size - row]
            }
        }
        return transposed_matrix
    }
    else if (option == 3)
    {
        col_size = size[1].toInt() - 1

        transposed_matrix = noInputMatrixBuilder(size)

        for (row in transposed_matrix.indices)
        {
            for (col in transposed_matrix[row].indices)
            {
                transposed_matrix[row][col] = matrix[row][col_size - col]
            }
        }
        return transposed_matrix
    }
    else if (option == 4)
    {
        row_size = size[0].toInt() - 1

        transposed_matrix = noInputMatrixBuilder(size)

        for (row in transposed_matrix.indices)
        {
            for (col in transposed_matrix[row].indices)
            {
                transposed_matrix[row][col] = matrix[row_size - row][col]
            }
        }
        return transposed_matrix
    }

    return matrix
}

// build matrix without user input
fun noInputMatrixBuilder(size : List<String>) : ArrayList<ArrayList<Double>>
{
    // funtion variables
    val i: Int
    val j: Int
    val matrix = arrayListOf<ArrayList<Double>>()
    var rows: ArrayList<Double>
    val zero : Int


    // initialize values
    zero = 0


    // set matriz size
    i = size[0].toInt()
    j = size[1].toInt()


    // build matrix
    for (row in 0 until i)
    {
        rows = arrayListOf()
        for (column in 0 until j)
        {
            rows.add(zero.toDouble())
        }
        matrix.add(rows)
    }

    return matrix
}


// calculate determinant
fun matrixDeterminant(size : List<String>, matrix: ArrayList<ArrayList<Double>>) : Double
{
    // function variables
    val original_row : Int
    var minor : ArrayList<ArrayList<Double>>
    var new_size : List<String>
    var determinant : Double


    // check for size
    original_row = size[0].toInt()


    if (original_row == 2)
    {
        return (matrix[0][0] * matrix[1][1]) - (matrix[0][1] * matrix[1][0])
    }
    else
    {
        determinant = 0.0

        for (col in matrix[0].indices)
        {
            new_size = listOf(((size[0].toInt() - 1).toString()), ((size[1].toInt() - 1).toString()))

            minor = matrixDeterminantBuilder(size, matrix, col)

            if(isEven(col))
            {
                determinant += matrix[0][col] * matrixDeterminant(new_size, minor)
            }
            else
            {
                determinant -= matrix[0][col] * matrixDeterminant(new_size, minor)
            }
        }

        return determinant
    }
}


// check for an even number
fun isEven(x : Int) : Boolean
{
    return x % 2 == 0
}


// build matrix for determinant calculus
fun matrixDeterminantBuilder(size : List<String>,
                             matrix : ArrayList<ArrayList<Double>>,
                             j : Int,
                             i : Int = 0) : ArrayList<ArrayList<Double>>
{
    // function variables
    val new_matrix : ArrayList<ArrayList<Double>>


    // create smaller matrix
    new_matrix = noInputMatrixBuilder(size)

    for (row in new_matrix.indices)
    {
        for (col in new_matrix[row].indices)
        {
            new_matrix[row][col] = matrix[row][col]
        }
    }

    new_matrix.remove(new_matrix[i])

    for (row in new_matrix.indices)
    {
        for (col in new_matrix[row].indices)
        {
            if (col == j)
            {
                new_matrix[row].remove(new_matrix[row][col])
            }
        }
    }

    return new_matrix
}


//inversing a matrix
fun inverseMatrix(size: List<String>,
                  matrix : ArrayList<ArrayList<Double>>) : ArrayList<ArrayList<Double>>
{
    // function variables
    val determinant : Double
    val cofactor_matrix : ArrayList<ArrayList<Double>>
    val transposed_cofactor_matrix : ArrayList<ArrayList<Double>>
    val inverse_matrix : ArrayList<ArrayList<Double>>


    // find determinant
    determinant = matrixDeterminant(size, matrix)


    // create matrix of cofactors
    cofactor_matrix = noInputMatrixBuilder(size)

    for (row in cofactor_matrix.indices)
    {
        for (col in cofactor_matrix[row].indices)
        {
            cofactor_matrix[row][col] = determinantOfMinors(row, col, matrix)

            if (isEven(row))
            {
                if (isEven(col))
                {
                    cofactor_matrix[row][col] = cofactor_matrix[row][col]
                }
                else
                {
                    cofactor_matrix[row][col] = -cofactor_matrix[row][col]
                }
            }
            else
            {
                if (isEven(col))
                {
                    cofactor_matrix[row][col] = -cofactor_matrix[row][col]
                }
                else
                {
                    cofactor_matrix[row][col] = cofactor_matrix[row][col]
                }
            }
        }
    }


    // transpose cofactor_matrix
    transposed_cofactor_matrix = transposeMatrix(1, size, cofactor_matrix)


    // calculate inverse
    inverse_matrix = matrixScalar((1/determinant), transposed_cofactor_matrix)


    return inverse_matrix
}


// calculate matrix of minors
fun determinantOfMinors(row : Int,
                 col : Int,
                 matrix : ArrayList<ArrayList<Double>>) : Double
{
    // function variables
    val size : List<String>
    val new_size : List<String>
    val new_matrix : ArrayList<ArrayList<Double>>
    val determinant : Double


    // get size of matrix
    size = listOf((matrix.size).toString(), (matrix.size).toString())


    // create new matrix to calculate determinant at said positions
    new_matrix = matrixDeterminantBuilder(size, matrix, col, row)


    // redifine size
    new_size = listOf((size[0].toInt() - 1).toString(), (size[1].toInt() - 1).toString())


    // return determinant for said position
    determinant = matrixDeterminant(new_size, new_matrix)

    return determinant
}


// print initial warning
fun printWarning()
{
    println("A calculator for matrices.")
    println("Please, use one and only one space to separate numbers.")
    println("Please, use one and only one backspace for entering a new line.")
    println("\n-----\n")
}

// print menu
fun menu() : Int
{
    // function variables
    val option : Int


    // print menu, receive input, return it
    println("1. Add Matrices")
    println("2. Multiply a matrix using a scalar")
    println("3. Multiply two matrices")
    println("4. Transpose a matrix")
    println("5. Calculate the determinant of a matrix")
    println("6. Inverse a matrix")
    println("0. Exit program")
    print("Your choice: > ")

    option = choice()

    printLine()

    return option
}


// exit program
fun exit()
{
    println("See you next time!")
    printLine()
    exitProcess(0)
}


// print line, dashes, line
fun printLine()
{
    println("\n-----\n")
}


// print dashes
fun dashes()
{
    println("-----")
}