fun addMatrices()
{
    // function variables
    var matrix1Size : List<String>
    var matrix1 : ArrayList<ArrayList<Double>>
    var matrix2Size : List<String>
    var matrix2 : ArrayList<ArrayList<Double>>
    val matrix3 : ArrayList<ArrayList<Double>>
    var flag : Boolean
    var flag1 : Boolean
    var flag2 : Boolean


    // initialize necessary values
    matrix1Size = listOf()
    matrix2Size = listOf()
    matrix1 = arrayListOf(arrayListOf())
    matrix2 = arrayListOf(arrayListOf())
    flag = true


    // get user input for matrices size
    while (flag)
    {
        print("Enter size of first matrix: > ")
        matrix1Size = readLine()?.split(" ")!!
        dashes()

        print("Enter size of second matrix: > ")
        matrix2Size = readLine()?.split(" ")!!
        dashes()


        // error check for equal size matrices
        flag1 = errorCheck(matrix1Size, matrix2Size)
        flag2 = addEqualMatrices(matrix1Size, matrix2Size)

        if (!flag1 && !flag2)
        {
            flag = false
        }
    }


    // get user input to build necessary matrices
    println("Enter first matrix values, row by row, column by column:")
    matrix1 = matrixBuilder(matrix1Size)
    dashes()

    println("Enter second matrix values, row by row, column by column:")
    matrix2 = matrixBuilder(matrix2Size)
    dashes()


    // print matrices and addition result
    println("First Matrix:")
    matrixPrinter(matrix1)
    dashes()

    println("Second Matrix:")
    matrixPrinter(matrix2)
    dashes()

    matrix3 = matricesAddition(matrix1, matrix2)

    println("The addition result is:")
    matrixPrinter(matrix3)
    printLine()
}


// add two matrices
fun matricesAddition(matrix1 : ArrayList<ArrayList<Double>>, matrix2 : ArrayList<ArrayList<Double>>) : ArrayList<ArrayList<Double>>
{
    // add two matrices, value by value
    for (row in matrix1.indices)
    {
        for (col in matrix1[row].indices)
        {
            matrix1[row][col] += matrix2[row][col]
        }
    }

    return matrix1
}