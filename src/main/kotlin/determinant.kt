fun determinant()
{
    // function variables
    var matrix1Size : List<String>
    val matrix1 : ArrayList<ArrayList<Double>>
    var flag :Boolean
    val determinant : Double


    // initialize variables
    flag = true
    matrix1Size = listOf()


    // get user input for matrix size
    while (flag)
    {
        print("Enter size of matrix: > ")
        matrix1Size = readLine()?.split(" ")!!
        dashes()

        flag = errorCheckSquare(matrix1Size)
    }

    // get user input to build necessary matrix
    println("Enter matrix values, row by row, column by column:")
    matrix1 = matrixBuilder(matrix1Size)
    dashes()


    // print matrix and determinant
    println("The matrix is:")
    matrixPrinter(matrix1)
    dashes()


    determinant = matrixDeterminant(matrix1Size, matrix1)
    println("The determinant is:")
    println(determinant)
    printLine()
}